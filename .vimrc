""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Author:
"   Marcheing - marcheing@gmail.com
"
" Description:
"   So this is me finally trying to do something about the craziness that is
"   my vimrc files spread across my machines. And it's actually my second try!
"   Warning: I do not recommend this .vimrc at all. I'm still figuring it out.
"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General Configurations:
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Warning: Vim, from version 8 onwards, has some default configurations
" already enabled. The ones I change in here are not present in my current
" defaults

" Hides the buffers when abandoned. Makes it harder to notice that you have
" hidden changes, so it's wiser to avoid the ! commands (:qa! and such). CoC
" has problems if this is not set, so I'm setting this here.
set hidden

" Backup makes a backup of the file before writing. We're turning this off
" because CoC has a an issue caused by some servers
set nobackup
set nowritebackup

" The number of lines for vim : commands. Two lines is enough for the error
" messages that may appear on CoC, and still allow for more commands to be
" typed
set cmdheight=2

" The time (milliseconds) after nothing happening for the file to be
" written. This is useful for CoC to check its things more often
set updatetime=300

" Enables 24-bit color on the terminal if supported. The colorscheme "gruvbox"
" requires it
set termguicolors

" Enables showing a status line on the last window. With a value of 2, it will
" always show the status line. With a plugin such as "Lightline", it enables
" the stylized status line
set laststatus=2

" Shows line numbers
set nu

" Allows use of the mouse. I need this for things that are much easier and
" simpler using the mouse, such as changing tabs (with lightline) and
" changing the height/width of windows
set mouse=a

" Search will be case insensitive until one needs it to be sensitive (such as
" when using an uppercase letter)
set smartcase

" Maps the command to exit neovim's terminal mode to CTRL+Q
tnoremap <C-q> <C-\><C-N>

" Expand tabs to spaces on:
" Haskell Files: The stack-ghci interpreter complains about tabs
au BufRead,BufNewFile *.hs set expandtab

set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Cursor target, basically. Highlights current line and column.
set cursorcolumn
set cursorline

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vim Plug Setup:
" Vim Plug is a minimalist Vim Plugin manager, and it provides what I need for
" my plugins
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes

" NERDtree is my favorite system explorer since it provides the minimal needed
" interaction and interface changes to provide a way of quickly changing
" files. In the future, some fuzzy-finder approach will be better
Plug 'scrooloose/nerdtree'

" Brightest highlights the word under the cursor. It's helpful to work on
" local code, when you're examining for local variables and parameter
" declarations, for example
Plug 'osyo-manga/vim-brightest'

" Gruvbox seemed like a nice and relaxing colorscheme. I might change it in
" the future, but I'd prefer not to download the vim-colorschemes plugin since
" I'd only use one of them at a time
Plug 'morhetz/gruvbox'

" Lightline displays a line at the bottom of the screen with useful
" information about the current file or buffer, such as: Current vim mode,
" encoding, and filetype
Plug 'itchyny/lightline.vim'

" Ale runs syntax checkers, linters, and compilers asynchronously and displays
" their messages on vim's interface. It's very useful to avoid wasting time
" compiling a file that may have a lot of syntax problems the programmer
" couldn't see.
Plug 'w0rp/ale'

" Provides rust filetype detection, syntax highlighting, and formatting (with
" rustfmt). The formatter is the main reason I've added this plugin.
Plug 'rust-lang/rust.vim'

" Finding a vim-only solution for haskell indent is hard. smartident and
" cindent are not enouh, so this plugin is the closest I could find.
Plug 'itchyny/vim-haskell-indent'

" This is an intelliSense-like engine for completion that needs other engines
" installed in order to work. It should also be enough to deprecate ALE, but
" I'm still testing.
Plug 'neoclide/coc.nvim', {'do': { -> coc#util#install()}}

" Neovim API for Haskell. Some Haskell plugins need this to work.
Plug 'neovimhaskell/haskell-vim'

" Stylish-Haskell formats Haskell code. There may be some differenfces between
" this and Haskell Indent, but I don't know what is it.
Plug 'alx741/vim-stylishask'

" Initialize plugin system
call plug#end()

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Colorscheme Configurations:
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
colorscheme gruvbox

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Rust Vim Configurations:
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Runs rustfmt when the rust file is saved. This prevents commits that are
" solely to change the formatting of code written without rustfmt
let g:rustfmt_autosave = 1

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NERDTree Configurations:
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Maps toggling of NERDTree to F2 in all modes
map <F2> :NERDTreeToggle<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Ale Configurations:
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Removes the loclist window that appears every time the lintes/checkers are
" run. The indicators on the file lines are usually enough, though...
" TODO: Add a keymapping to display the loclist
let g:ale_set_loclist = 0

" This is the complete list of haskell linters without stack-ghc, since it was
" generating warnings of ambiguous definitions on default-generated projects
let g:ale_linters = {'haskell': ['cabal_ghc', 'ghc', 'ghc_mod', 'hdevtools',
			\'hie', 'hlint', 'stack_build']}

" Use rls for rust projects
let g:ale_linters = {'rust': ['rls']}
let g:ale_fixers = {'rust': ['rustfmt'], 'python': ['black']}

let g:ale_rust_rls_config = {'rust': { 'clippy_preference': 'on' }}
let g:ale_fix_on_save = 1

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Coc Configurations:
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>'

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Use TAB to trigger completion
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Also use CTRL+Space to trigger completion
inoremap <silent><expr> <c-space> coc#refresh()

" Use Enter to confirm selection on completion
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Rename current word
nmap <leader>rn <Plug>(coc-rename)

" Format current region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)
